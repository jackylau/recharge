/***************************************************
 ** @Desc : This file for 初始化模型
 ** @Time : 2019.03.30 16:22 
 ** @Author : Joker
 ** @File : init_models
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.03.30 16:22
 ** @Software: GoLand
****************************************************/
package models

import (
	"github.com/astaxie/beego/orm"
)

func init() {
	//注册表模型
	orm.RegisterModel(new(UserInfo))
	orm.RegisterModel(new(Merchant))
	orm.RegisterModel(new(RechargeRecord))
	orm.RegisterModel(new(WithdrawRecord))
	orm.RegisterModel(new(TransferRecord))
	orm.RegisterModel(new(BankInfo))
	orm.RegisterModel(new(UserHistory))
	orm.RegisterModel(new(UserInfoExpansion))
	orm.RegisterModel(new(CustomPlusMinus))
}

func UserInfoTBName() string {
	return "user_info"
}

func MerchantTBName() string {
	return "merchant"
}

func RechargeRecordTBName() string {
	return "recharge_record"
}

func WithdrawRecordTBName() string {
	return "withdraw_record"
}

func TransferRecordTBName() string {
	return "transfer_record"
}

func BankInfoTBName() string {
	return "bank_info"
}

func UserHistoryTBName() string {
	return "user_history"
}

func UserInfoExpansionTBName() string {
	return "user_info_expansion"
}

func CustomPlusMinusTBName() string {
	return "custom_plus_minus"
}

